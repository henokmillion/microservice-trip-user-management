const Member = require('../models/member.model')

module.exports.getMembers = () => {
    return new Promise((succeed, fail) => {
        Member.find({ status: "active" }, (err, members) => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else {
                succeed({
                    status: 200,
                    data: members
                })
            }
        })
    })
}

module.exports.addMember = (member) => {
    return new Promise((succeed, fail) => {
        const _member = new Member(member)
        _member.status = "active"
        _member.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else {
                succeed({
                    status: 201,
                    msg: "successfully saved member"
                })
            }
        })
    })
}

module.exports.removeMemberById = (id) => {
    return new Promise((succeed, fail) => {
        Member.findOne({ _id: id, status: "active" }, (err, member) => {
            console.log(member)
            if (member) {
                member.status = "inactive"
                member.save(err => {
                    if (err) {
                        fail({
                            status: 500,
                            error: err
                        })
                    } else if (member) {
                        succeed({
                            status: 200,
                            msg: "successfully removed member"
                        })
                    } else {
                        fail({
                            status: 404,
                            msg: `no member found with id ${id}`
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    msg: `no member found with id ${id}`
                })
            }
        })
    })
} 
