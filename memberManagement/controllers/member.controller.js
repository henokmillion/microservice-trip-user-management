const memberService = require('../services/member.service')

module.exports.getMembers = (req, res, next) => {
    memberService.getMembers()
        .then(response => res.status(response.status).json(response))
        .catch(err => res.status(err.status).json(err))
}

module.exports.addMembers = (req, res, next) => {
    const valid = !!req.body.member
    if (valid) {
        memberService.addMember(req.body.member)
            .then(response => res.status(response.status).json(response))
            .catch(err => res.status(err.status).json(err))
    } else {
        res.status(400).json({ status: 400, msg: "Error: Incomplete request body" })
    }
}

module.exports.removeMembers = (req, res, next) => {
    const valid = !!req.params.id
    if (valid) {
        memberService.removeMemberById(req.params.id)
            .then(response => res.status(response.status).json(response))
            .catch(err => res.status(err.status).json(err))
    } else {
        res.status(400).json({ status: 400, msg: "Error: Incomplete request" })
    }
}
