const express = require('express')
const router = express.Router()
const memberController = require('../controllers/member.controller')

router.get('/', memberController.getMembers)

router.post('/', memberController.addMembers)

router.delete('/:id', memberController.removeMembers)

module.exports = router