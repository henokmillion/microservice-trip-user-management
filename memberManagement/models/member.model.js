const mongoose = require('mongoose')

module.exports = mongoose.model("Member", new mongoose.Schema({
    name: String,
    department: String,
    batch: Number,
    subdepartment: String,
    school_department: String,
    // spiritual_father: String,
    phone: String,
    status: String
}))
