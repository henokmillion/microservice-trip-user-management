const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const memberRoute = require('./routes/member.route')
const mongoose = require('mongoose')
const config = require('./config')

mongoose.connect(config.database, { useMongoClient: true })
mongoose.connection.on('connected', () => console.log('connected to db'))

app.use(bodyParser.json())
app.use('/api/v1/member', memberRoute)


const server = app.listen(8000, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`memberManagement running on ${host}:${port}`)
})

module.exports = app
