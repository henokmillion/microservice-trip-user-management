# MicroService Based User and Trip Management
## Description
This is a simple trip and user management that uses 
Microservices for __user__ and __trip__ management

## Getting Started
run `node app.js`
the server will begin serving on `localhost:8002`