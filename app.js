const tripManagement = require('./tripManagment/app')
const memberManagement = require('./memberManagement/app')
const fetch = require('node-fetch')
const ejs = require('ejs')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
memberManagement
tripManagement

app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

// trip management routes
app.get('/trip', (req, res, next) => {
    fetch('http://localhost:8001/api/v1/trip', {
        method: "get",
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => response.json())
        .then(response => {
            console.log(response)
            res.render('partials/trip/trips', { trips: response.data })
        })
})
app.get('/trip/add-new', (req, res, next) => {
    res.render('partials/trip/add-new')
})
app.post('/trip/add-new', (req, res, next) => {
    console.log(typeof req.body)
    fetch('http://localhost:8001/api/v1/trip', {
        method: "post",
        body: JSON.stringify(req.body),
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => console.log(response.status))
        .then(response => res.render('partials/trip/add-new'))
})


// member management routes
app.get('/member', (req, res, next) => {
    fetch('http://localhost:8000/api/v1/member', {
        method: "get",
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => response.json())
        .then(response => {
            console.log(response)
            res.render('partials/member/members', { members: response.data })
        })
})
app.get('/member/add-new', (req, res, next) => {
    res.render('partials/member/add-new')
})
app.post('/member/add-new', (req, res, next) => {
    console.log(typeof req.body)
    fetch('http://localhost:8000/api/v1/member', {
        method: "post",
        body: JSON.stringify(req.body),
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => console.log(response.status))
        .then(response => res.render('partials/member/add-new'))
})


app.get('/', (req, res, next) => {
    res.render('index')
})




const server = app.listen(8002, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app is running on ${host}:${port}`)
})

fetch('http://localhost:8000/api/v1/trip').then(response => console.log(response))

