const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const tripRoute = require('./routes/trip.route')
const mongoose = require('mongoose')
const config = require('./config')

mongoose.connect(config.database, { useMongoClient: true })
mongoose.connection.on('connected', () => console.log('connected to db'))

app.use(bodyParser.json())
app.use('/api/v1/trip', tripRoute)


const server = app.listen(8001, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`tripManagement running on ${host}:${port}`)
})

module.exports = app
