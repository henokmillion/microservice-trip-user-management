const tripService = require('../services/trip.service')

module.exports.getTrips = (req, res, next) => {
    tripService.getTrips()
    .then(response => res.status(response.status).json(response))
    .catch(err => res.status(err.status).json(err))
}

module.exports.addTrip = (req, res, next) => {
    const valid = !!req.body.trip
    if (valid) {
        tripService.addTrip(req.body.trip)
        .then(response => res.status(response.status).json(response))
        .catch(err => res.status(err.status).json(err))
    } else {
        res.status(400).json({status: 400, msg: "Error: Incomplete request body"})
    }
}

module.exports.removeTrip = (req, res, next) => {
    const valid = !!req.params.id
    if (valid) {
        tripService.removeTripById(req.params.id)
        .then(response => res.status(response.status).json(response))
        .catch(err => res.status(err.status).json(err))
    } else {
        res.status(400).json({status: 400, msg: "Error: Incomplete request parameters"})
    }
}
