const Trip = require('../models/trip.model')

module.exports.getTrips = () => {
    return new Promise((succeed, fail) => {
        Trip.find((err, trips) => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else if (trips) {
                succeed({
                    status: 200,
                    data: trips
                })
            } else {
                fail({
                    status: 404,
                    msg: "No trips found"
                })
            }
        })
    })
}

module.exports.addTrip = (trip) => {
    return new Promise((succeed, fail) => {
        trip.status = "active"
        const _trip = new Trip(trip)
        _trip.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else {
                succeed({
                    status: 201,
                    msg: "successfully added trip"
                })
            }
        })
    })
}

module.exports.removeTripById = (id) => {
    return new Promise((succeed, fail) => {
        Trip.findOne({
            _id: id,
            status: "active"
        }, (err, trip) => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })      
            } else if (trip) {
                trip.status = "inactive"
                trip.save(_err => {
                    if (_err) {
                        fail({
                            status: 500,
                            error: _err
                        })
                    } else {
                        succeed({
                            status: 200,
                            msg: "successfully removed trip"
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    msg: "No trips found"
                })
            }
        })
    })
}
