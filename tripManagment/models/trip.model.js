const mongoose = require('mongoose')

module.exports = mongoose.model("Trip", new mongoose.Schema({
    place: String,
    time: Date,
    batch: Number,
    status: String
}))