const express = require('express')
const tripController = require('../controllers/trip.controller')
const tripRoute = express.Router()

tripRoute.get('/', tripController.getTrips)
tripRoute.post('/', tripController.addTrip)
tripRoute.delete('/:id', tripController.removeTrip)

module.exports = tripRoute